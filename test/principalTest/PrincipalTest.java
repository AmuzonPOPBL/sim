package principalTest;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import Almacen.Almacen;
import modelo.all.Robot;
import principal.Principal;

public class PrincipalTest {

	Principal p;
	Almacen a;
	
	@Before 
	public void init() {
		a = new Almacen();
		p = new Principal();
		
	}
	
	@Test
	public void testGetListaRobots() {
		Robot r = p.getControladorRobots().getListaRobots().get(0);
		assertEquals(1, r.getrId());
	}
	@Test
	public void testSetListaRobots() {
		List<Robot> l = p.getControladorRobots().getListaRobots();
		p.getControladorRobots().setListaRobots(l);
		assertEquals(l, p.getControladorRobots().getListaRobots());
	}
	@Test
	public void testGetListaSegmentos() {
		a.getListaSegmentos();
		p.getListaSegmentos();
		assertEquals(10, p.getListaSegmentos().getSize());
	}
	
	@Test
	public void testGetListaRobotsPrincipal() {
		a.getListaRobots();
		p.getListaRobots();
		assertEquals(6, p.getListaRobots().size());
	}

}

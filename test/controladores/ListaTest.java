package controladores;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import Almacen.Almacen;
import listas.ListaSegmento;
import modelo.all.Segmento;

public class ListaTest {

	Almacen a;
	ListaSegmento lista;
	@Before
	public void init() {
		
		a = new Almacen();
		lista  = a.getListaSegmentos();
	}
	@Test
	public void testGetSize() {
		
		assertEquals(10, lista.getSize());
	}
	@Test
	public void testRemove() {
		lista.remove(9);
		assertEquals(9, lista.getSize());
	}
	@Test
	public void testSegmentoById() {
		assertEquals(1, lista.getSegmentoById(1).getsId());
		assertEquals(null, lista.getSegmentoById(0));
	}
	@Test
	public void testSetListaSegmentos() {
		
		List<Segmento> l = lista.getListaSegmentos();
		lista.setListaSegmentos(l);
		
		assertEquals(l.get(0), lista.getListaSegmentos().get(0));
	}

}

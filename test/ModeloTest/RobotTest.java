package ModeloTest;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import Almacen.Almacen;
import controladores.ControladorRuta;
import modelo.all.Robot;
import modelo.all.Segmento;
import modelo.all.WorkStation;

public class RobotTest {

	public Robot robot ,r1;
	Segmento s1,s2;
	ControladorRuta rute;
	Almacen almacen;
	/**
	 * @brief Method to cretate objects
	 */
	@Before
	public void crearRobot()
	{
		almacen = new Almacen();
		robot = new Robot();
		//robot = new Robot();
		r1 = new Robot(1,"esperando", almacen.getListaSegmentos().getListaSegmentos(), almacen.getControladorRuta());
	}
	
	@Test
	public void testGetWkActual() {
		
		assertEquals(null, robot.getWkActual());
		
	}
	
	@Test
	public void testGetSgActual() {
		
		assertEquals(null, robot.getSegActual());
		
	}
	@Test
	public void testGetOrden() {
		
		assertEquals(null, robot.getOrden());
		
	}
	@Test
	public void testGetDestino() {
		
		assertEquals(null, robot.getDestino());
		
	}
	
	@Test
	public void testSetDestino() {
		robot.setDestino("104");
		assertEquals("104", robot.getDestino());
		
	}
	@Test
	public void testSetOrder() {
		robot.setOrden("104");
		assertEquals("104", robot.getOrden());
		
	}
	@Test
	public void testSetRobotState() {
		robot.setRobotState("esperando");
		assertEquals("esperando", robot.getRobotState());
		
	}
	@Test
	public void testSetOrden() {
		robot.setOrden("101", "104");
		assertEquals("moviendose", robot.getRobotState());
			
	}
	@Test
	public void testSetPosicion2() {
		Robot r = almacen.getListaRobots().get(0);
		r.setPosicion2();
		assertEquals(r, almacen.getListaRobots().get(0));
			
	}
	
	@Test
	public void testSetPosicionID() {
		Robot r = almacen.getListaRobots().get(0);
		r.setPositionID(2);
		assertEquals(2, r.getPositionID());
			
	}
	
	@Test
	public void testSetWkActual() {
		Robot r = almacen.getListaRobots().get(0);
		r.setWkActual(almacen.getListaWk().get(1));
		assertEquals(almacen.getListaWk().get(1), r.getWkActual());
			
	}
	
	@Test
	public void testGetRutero() {
		Robot r = almacen.getListaRobots().get(0);		
		assertEquals(almacen.getListaRobots().get(0).getRutero(), r.getRutero());
			
	}
	
	@Test
	public void testSetRutero() {
		Robot r = almacen.getListaRobots().get(0);
		r.setRutero(almacen.getControladorRuta());
		assertEquals(almacen.getListaRobots().get(0).getRutero(), r.getRutero());
			
	}
	
	@Test
	public void testSetPosition1Way() {
		Robot r = almacen.getListaRobots().get(0);
		r.setSegActual(null);	
		assertEquals("id_sg= " + null + ", id_ws= " + null,r.setPosicion());			
	}
	
	@Test
	public void testSetPosition2Way() {
		Robot r = almacen.getListaRobots().get(0);
		Segmento s = almacen.getListaSegmentos().getElementAt(0);
		r.setSegActual(s);
		r.setWkActual(null);
		assertEquals("id_sg=" + r.getSegActual().getsId() + ", id_ws='" + null,r.setPosicion());			
	}
	
	@Test
	public void testSetPosition3Way() {
		Robot r = almacen.getListaRobots().get(0);
		Segmento s = almacen.getListaSegmentos().getElementAt(0);
		WorkStation wk = almacen.getListaWk().get(0);
		r.setSegActual(s);
		r.setWkActual(wk);
		assertEquals("id_sg=" + r.getSegActual().getsId() + ", id_ws='" + r.getWkActual().getId() + "'",r.setPosicion());			
	}
	
	@Test
	public void testBuscarParking() {
		Robot r = almacen.getListaRobots().get(0);
		r.buscarParking();
		assertEquals("parado", r.getRobotState());			
	}
	
	
	
	
	
	
	

}

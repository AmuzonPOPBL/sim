package ModeloTest;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Almacen.Almacen;
import modelo.all.Robot;
import modelo.all.Segmento;
import modelo.all.WorkStation;

public class SegmentTest {
	
	Almacen a;
	Segmento s;
	@Before
	public void init() {
		a = new Almacen();
		s = a.getListaSegmentos().getElementAt(0);
		Segmento seg = new Segmento();
	}
	
	@Test
	public void testGetId() {
		
		s.setsId(1);
		assertEquals(1 , s.getsId());	
	}

	@Test
	public void testGetRobotEnSeg() {
		Robot r = a.getListaRobots().get(0);
		s.setRobotEnSeg(r);
		assertEquals(r , s.getRobotEnSeg());	
	}
	
	@Test
	public void testIsFull() {
		s.setFull(true);
		assertEquals(true , s.isFull());	
	}
	
	@Test
	public void testGetIn() {
		Robot r = a.getListaRobots().get(0);
		s.setFull(false);
		s.getIn(r);
		
		assertEquals(true , s.isFull());	
	}
	
	@Test
	public void testGetOut() {
		s.getOut();
		assertEquals(false , s.isFull());	
	}
	
	@Test
	public void testSetWk() {
		WorkStation w =a.getListaWk().get(0);
		s.setWorkstation(w);
		assertEquals(w , s.getWorkstation());	
	}

}

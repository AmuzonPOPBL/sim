package ModeloTest;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Almacen.Almacen;
import modelo.all.Robot;
import modelo.all.Segmento;
import modelo.all.WorkStation;

public class WorkStationTesting {

	WorkStation workStation,w;
	Almacen a;
	@Before
	public void init() {
		a = new Almacen();
		w = a.getListaWk().get(0);
		workStation = new WorkStation("104",false);
	}
	@Test
	public void testGetIn1() {
		Robot r = a.getListaRobots().get(0);
		w.setFull(false);
		w.getIn(r);
		assertEquals(true, w.isFull());
	}

	@Test
	public void testGetOut() {
		workStation.getOut();
		assertEquals(false, workStation.isFull());
	}
	
	@Test
	public void testRobotEnWst() {
		Robot r = a.getListaRobots().get(0);
		w.setRobotEnWSt(r);
		
		workStation.getOut();
		assertEquals(r, w.getRobotEnWSt());
	}
	
	@Test
	public void testIsParking() {
		w.setParking(true);
		assertEquals(true, w.isParking());
	}
	
	@Test
	public void testGetIdSeg() {
		assertEquals(1, w.getIdSegmento());
	}
	
	@Test
	public void testToString() {
		w.toString();
		assertEquals(1, w.getIdSegmento());
	}
	
}

package ModeloTest;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import modelo.all.Order;

public class OrderTesting {

	Order order;
	@Before
	public void init() {
		
		order = new Order(1,1,"104");
	}
	
	
	@Test
	public void testGetOrderId() {
		
		assertEquals(1, order.getIdOrder());	
		
	}
	@Test
	public void testGetProductoId() {
		
		assertEquals(1, order.getIdProducto());
		
	}
	@Test
	public void testGetDestio() {
		
		assertEquals("104", order.getDestino());		
	}
	@Test
	public void testSetOrderId() {
		order.setIdOrder(2);
		assertTrue(order.getIdOrder() == 2);
	}
	@Test
	public void testSetDestino() {		
		order.setDestino("103");
		assertEquals("103",order.getDestino());		
	}
	@Test
	public void testSetpProductId() {		
		order.setIdProducto(2);
		assertTrue(order.getIdProducto() == 2);		
	}
	

}

package controladores;
/** @file ControladorRutajava
* @brief Controller that manages the paths
* @authors
* Name | Surname     | Email                                 |
* -------------      | -----------------                     | ---------------------------------------------------|
* Aitor | Pi�eiro    | aitor.pineiro@alumni.mondragon.edu    |
* Ander | Rekalde    | ander.recalde@alumni.mondragon.edu    |
* Ander | Rementeria | ander.rementeria@alumni.mondragon.edu |
* Anton | Alberdi    | anton.alberdi@alumni.mondragon.edu    |
* @date 14/01/2019
*/
import java.util.ArrayList;
import java.util.List;
import conectionMySQL.OrderConectionMySQL;
import listas.ListaSegmento;
import modelo.all.Order;
import modelo.all.Robot;
import modelo.all.Segmento;


public class ControladorRuta extends Thread {
	OrderConectionMySQL orderConection;
	List<Robot> listaRobots;
	ListaSegmento listaSegmentos;
	List<Order> listaPedidos;
	List<Segmento> lista = new ArrayList<Segmento>();
	boolean end = false, recogido = false;
	int origen;
	boolean done = false;
	int kont,kontMax;

	public ControladorRuta(List<Order> listaO, List<Robot> listaR, ListaSegmento listaS,
			OrderConectionMySQL orderConection) {
		this.listaRobots = listaR;
		this.listaSegmentos = listaS;
		this.listaPedidos = listaO;
		this.orderConection = orderConection;
		this.kont=0;
	}


	/**Method that returns the only robot that is not moving
	*@param No parameter
	*@return Robot
	*/
	public Robot getFreeRobot() {
		Robot aux = null;
		for (Robot r : listaRobots) {
			if (r.getRobotState().equals("parado")) aux = r;
		}
		return aux;
	}

	/**Method that having the source position and the destiny of a robot calculates a route that will follow
	*@param String idGo, String destino, Robot r
	*@return List<Segmento>
	*/
	public List<Segmento> calcularRuta(String idGo, String destino, Robot r) {
		if ((r.getWkActual() != null) || (r.getSegActual() != null)) origen = r.getWkActual().getIdSegmento();
		else origen = 8;

		while (!end) {
			switch (origen) {
			case 1:
				lista.add(listaSegmentos.getSegmentoById(2));
				origen = 2;
				
				break;
			case 2:
				if (recogido == true && listaSegmentos.getSegmentoById(2).getWorkstation().getId().equals(destino)) {
					end = true;
				} else {
					if (listaSegmentos.getSegmentoById(2).getWorkstation().getId().equals(idGo)) {
						recogido = true;
					}
					if (recogido == false && idGo.equals("104")) {
						lista.add(listaSegmentos.getSegmentoById(9));
						origen = 9;
					}
					if (recogido == true && destino.equals("104")) {
						lista.add(listaSegmentos.getSegmentoById(9));
						origen = 9;
					}

				}
				if (origen == 2) {
					lista.add(listaSegmentos.getSegmentoById(3));
					origen = 3;
				}
				break;
			case 3:
				lista.add(listaSegmentos.getSegmentoById(4));
				origen = 4;
				break;
			case 4:
				if (recogido == true && listaSegmentos.getSegmentoById(4).getWorkstation().getId().equals(destino)) {
					end = true;
				} else {
					if (listaSegmentos.getSegmentoById(4).getWorkstation().getId().equals(idGo)) {
						recogido = true;
					}
					lista.add(listaSegmentos.getSegmentoById(5));
					origen = 5;
				}
				break;
			case 5:
				lista.add(listaSegmentos.getSegmentoById(6));
				origen = 6;
				break;
			case 6:
				if (recogido == true && listaSegmentos.getSegmentoById(6).getWorkstation().getId().equals(destino)) {
					end = true;
				} else {
					if (listaSegmentos.getSegmentoById(6).getWorkstation().getId().equals(idGo)) {
						recogido = true;
					}
					if (recogido == false && idGo.equals("102")) {
						lista.add(listaSegmentos.getSegmentoById(10));
						origen = 10;
					}
					if (recogido == true && destino.equals("102")) {
						lista.add(listaSegmentos.getSegmentoById(10));
						origen = 10;
					}
					if (origen == 6) {
						lista.add(listaSegmentos.getSegmentoById(7));
						origen = 7;
					}

				}

				break;
			case 7:
				lista.add(listaSegmentos.getSegmentoById(8));
				origen = 8;
				break;
			case 8:
				if (recogido == true && listaSegmentos.getSegmentoById(8).getWorkstation().getId().equals(destino)) {
					end = true;
				} else {
					if (listaSegmentos.getSegmentoById(8).getWorkstation().getId().equals(idGo)) {
						recogido = true;
					}
					lista.add(listaSegmentos.getSegmentoById(1));
					origen = 1;
				}
				break;
			case 9:
				lista.add(listaSegmentos.getSegmentoById(8));
				origen = 8;
				break;
			case 10:
				lista.add(listaSegmentos.getSegmentoById(4));
				origen = 4;
				break;
			}
		}

		return lista;
	}

	public boolean isDone() {
		return done;
	}

	public void setDone(boolean done) {
		this.done = done;
	}

	public int getKont() {
		return kont;
	}

	public void setKont(int kont) {
		this.kont = kont;
	}

	public int getKontMax() {
		return kontMax;
	}

	public void setKontMax(int kontMax) {
		this.kontMax = kontMax;
	}
	
	public void incKont() {
		this.kont++;
	}
	
	/**Method that check the status of the barrier and changes it when it takes the maximum value
	*@param No parameter
	*@return Void method
	*/
	public synchronized void checkBarrier() {
		if(kont == kontMax) {
			this.setDone(true);
			this.notifyAll();
		}
	}
	/**Method that creates a order. assigns an order to a robot and stablish the path to follow.
	*@param No parameter
	*@return Robot
	*/
	public synchronized void crearOrden(String idGo, String destino, Order o) {
		Robot robotOrden;
		List<Segmento> ruta;

		robotOrden = getFreeRobot();
		if (robotOrden == null) {
			System.out.println("No hay robots disponibles");
		} else {
			ruta = calcularRuta(idGo, destino, robotOrden);
			robotOrden.setOrden(idGo, destino);
			robotOrden.setRuta(ruta);
			orderConection.updateRobotOrder(o, robotOrden.getrId());
			if (robotOrden.isAlive()) {
				robotOrden.setRutero(this);
				robotOrden.getWkActual().despertar();
			} else {
				robotOrden.start();
				robotOrden.setRutero(this);
			}
		}
	}
	/**Mthod that stops when all the orders are completed
	*@param No parameter
	*@return Void method
	*/
	@Override
	public synchronized void run() {
		this.setDone(false);
		this.setKont(0);
		this.setKontMax(listaPedidos.size());
		
		if (listaPedidos != null) {
			for (Order o : listaPedidos) {
				String idGO = orderConection.getOrigenProducto(o);
				String destino = o.getDestino();
				crearOrden(idGO, destino, o);
			}
		}
		while (!done) {
			try {
				this.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

}
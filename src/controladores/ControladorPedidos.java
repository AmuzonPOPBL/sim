package controladores;
/** @file ControladorPedidos.java
* @brief Controller that manages the orders
* @authors
* Name | Surname     | Email                                 |
* -------------      | -----------------                     | ---------------------------------------------------|
* Aitor | Pi�eiro    | aitor.pineiro@alumni.mondragon.edu    |
* Ander | Rekalde    | ander.recalde@alumni.mondragon.edu    |
* Ander | Rementeria | ander.rementeria@alumni.mondragon.edu |
* Anton | Alberdi    | anton.alberdi@alumni.mondragon.edu    |
* @date 14/01/2019
*/
import java.util.List;

import conectionMySQL.OrderConectionMySQL;
import modelo.all.Order;
import principal.Principal;

public class ControladorPedidos extends Thread {

	Principal principal;
	OrderConectionMySQL orderConection;
	ControladorRuta controladorR;
	List<Order> listaOrders;

	public ControladorPedidos(Principal principal) {
		orderConection = new OrderConectionMySQL();
		this.principal=principal;
	}
	
	/**Method that sleeps the execution
	*@param int ms
	*@return Void method
	*/
	private void waitMS(int ms) {
		try {
			Thread.sleep(ms);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**Method that sends the list of orders to the path controller and
	*waits until its finished to make an update in the database
	*@param No parameter
	*@return Void method
	*/
	@Override
	public void run() {
		do {
			listaOrders = orderConection.loadAllOrders();
			if (listaOrders != null && listaOrders.size() != 0) {
				controladorR = new ControladorRuta(listaOrders,principal.getListaRobots(),principal.getListaSegmentos(),orderConection);
				controladorR.start();
				try {
					controladorR.join();
				} catch (InterruptedException e) {
					e.printStackTrace(); }
				for(Order o: listaOrders) orderConection.updateFinishedOrders(o);
			}
			waitMS(2000);
		} while (true);
	}

}

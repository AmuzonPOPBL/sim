package controladores;
/** @file ControladorSegmentos.java
* @brief Controller that manages the segments status
* @authors
* Name | Surname     | Email                                 |
* -------------      | -----------------                     | ---------------------------------------------------|
* Aitor | Pi�eiro    | aitor.pineiro@alumni.mondragon.edu    |
* Ander | Rekalde    | ander.recalde@alumni.mondragon.edu    |
* Ander | Rementeria | ander.rementeria@alumni.mondragon.edu |
* Anton | Alberdi    | anton.alberdi@alumni.mondragon.edu    |
* @date 14/01/2019
*/
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import modelo.all.Segmento;

public class ControladorSegmento {

	List<Segmento> listaSegmentos;
	/**Constructor where a listener is added to the segments that notifies when it changes
	*@param int ms
	*@return Void method
	*/
	public ControladorSegmento(List<Segmento> listaSegmentos, PropertyChangeListener listener) {
		super(); 
		this.listaSegmentos = new ArrayList<>(listaSegmentos);
		for(Segmento s : listaSegmentos) s.addPropertyChangeListener(listener);
	}
	public List<Segmento> getListaSegmentos() {
		return listaSegmentos;
	}
	public void setListaSegmentos(List<Segmento> listaSegmentos) {
		this.listaSegmentos = listaSegmentos;
	}

	
}

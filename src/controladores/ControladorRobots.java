package controladores;
/** @file ControladorRobots.java
* @brief Controller that manages the robots
* @authors
* Name | Surname     | Email                                 |
* -------------      | -----------------                     | ---------------------------------------------------|
* Aitor | Pi�eiro    | aitor.pineiro@alumni.mondragon.edu    |
* Ander | Rekalde    | ander.recalde@alumni.mondragon.edu    |
* Ander | Rementeria | ander.rementeria@alumni.mondragon.edu |
* Anton | Alberdi    | anton.alberdi@alumni.mondragon.edu    |
* @date 14/01/2019
*/
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import modelo.all.Robot;


public class ControladorRobots extends Thread{
	List<Robot> listaRobots;
	//añade listener a Segmentos

	/**Constructor where a listener is added to the robots that notifies when it changes
	*@param int ms
	*@return Void method
	*/
	public ControladorRobots(List<Robot> listaRobots, PropertyChangeListener listener) {
		super();
		this.listaRobots = new ArrayList<> (listaRobots);
		for(Robot r : listaRobots) r.addPropertyChangeListener(listener); 
	}
	public List<Robot> getListaRobots() {
		return listaRobots;
	}
	public void setListaRobots(List<Robot> listaRobots) {
		this.listaRobots = listaRobots;
	}

}

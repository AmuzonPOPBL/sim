package Almacen;

import java.util.ArrayList;
import java.util.List;

import conectionMySQL.OrderConectionMySQL;
import conectionMySQL.RobotConectionMySQL;
import controladores.ControladorPedidos;

import controladores.ControladorRuta;

import listas.ListaSegmento;
import modelo.all.Order;
import modelo.all.Robot;
import modelo.all.Segmento;
import modelo.all.WorkStation;
import principal.Principal;

public class Almacen {

	RobotConectionMySQL robotConection;
	List<Robot> listaRobots;
	List<WorkStation> listaWk;
	List<Order> listaOrders;
	ListaSegmento listaSegmentos;
	Principal p;
	OrderConectionMySQL orderConection;

	ControladorPedidos controladorPedidos;
	ControladorRuta rutero;

	public Almacen() {
		inicializarSegmentos();
		inicializarRobots();
		
		inicializarOrders();
		orderConection = new OrderConectionMySQL();
		rutero = new ControladorRuta(listaOrders, listaRobots, listaSegmentos, orderConection);	
		controladorPedidos = new ControladorPedidos(p);
		p = new Principal();
	}

	public void inicializarRobots() {
		listaRobots = new ArrayList<Robot>();
		for (int i = 1; i <= 6; i++) {
			listaRobots.add(new Robot(i, "parado", listaSegmentos.getListaSegmentos(), rutero));
		}
	}

	public void inicializarSegmentos() {

		crearWorkStations();

		listaSegmentos = new ListaSegmento();
		for (int i = 1; i <= 10; i++) {
			if (i < 9) {
				listaSegmentos.add(new Segmento(i, null, listaWk.get(i - 1)));
			} else {
				listaSegmentos.add(new Segmento(i, null, null));
			}
		}
		
		enlazarWk();
	}

	public void enlazarWk() {
		int cont = 0;
		for (Segmento s : listaSegmentos.getListaSegmentos()) {
			if (cont < 8) {
				s.getWorkstation().setIdSegmento(s.getsId());
			}
			cont++;
		}
	}

	public void crearWorkStations() {

		boolean park = true;
		listaWk = new ArrayList<WorkStation>();
		int z = 1;
		for (int i = 1; i <= 8; i++) {
			if (park) {
				listaWk.add(new WorkStation("p0" + z, park));
				park = false;
			} else {
				listaWk.add(new WorkStation("10" + z, park));
				park = true;
				z++;
			}
		}
	}

	public void inicializarOrders() {
		listaOrders = new ArrayList<Order>();
		for (int i = 1; i < 6; i++) {
			listaOrders.add(new Order(i, i + 9, "104"));
		}
	}

	
	public RobotConectionMySQL getRobotConection() {
		return robotConection;
	}

	public List<Robot> getListaRobots() {
		return listaRobots;
	}

	public List<WorkStation> getListaWk() {
		return listaWk;
	}

	public List<Order> getListaOrders() {
		return listaOrders;
	}

	public ListaSegmento getListaSegmentos() {
		return listaSegmentos;
	}

	public OrderConectionMySQL getOrderConection() {
		return orderConection;
	}

	public ControladorPedidos getControladorPedidos() {
		return controladorPedidos;
	}

	public ControladorRuta getControladorRuta() {
		return rutero;
	}

	public Principal getP() {
		return p;
	}
}

package listas;
/** @file ListaSegmento.java
* @brief Object that acts as a list of segments
* @authors
* Name | Surname     | Email                                 |
* -------------      | -----------------                     | ---------------------------------------------------|
* Aitor | Pi�eiro    | aitor.pineiro@alumni.mondragon.edu    |
* Ander | Rekalde    | ander.recalde@alumni.mondragon.edu    |
* Ander | Rementeria | ander.rementeria@alumni.mondragon.edu |
* Anton | Alberdi    | anton.alberdi@alumni.mondragon.edu    |
* @date 15/01/2019
*/
import java.util.ArrayList;
import java.util.List;
import javax.swing.ListModel;
import javax.swing.event.ListDataListener;
import modelo.all.Segmento;



public class ListaSegmento implements ListModel <Segmento>{
	List<Segmento> listaSegmentos;
	
	public ListaSegmento(){
		listaSegmentos = new ArrayList<>();
	}
	
	public int getSize() {
		return listaSegmentos.size();
		
	}

	public Segmento getElementAt(int index) {
		return listaSegmentos.get(index);
	}

	public void remove(int indice) {
		listaSegmentos.remove(indice);
	}

	public void add(Segmento s) {
		listaSegmentos.add(s);
	}

	
	public Segmento getSegmentoById(Integer id) {
		for(Segmento s : listaSegmentos) {
			if(s.getsId()==id) {
				return s;
			}
		}
		return null;
	}

	public List<Segmento> getListaSegmentos() {
		return listaSegmentos;
	}

	public void setListaSegmentos(List<Segmento> listaSegmentos) {
		this.listaSegmentos = listaSegmentos;
	}

	@Override
	public void addListDataListener(ListDataListener l) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeListDataListener(ListDataListener l) {
		// TODO Auto-generated method stub
		
	}
	
	
}

	
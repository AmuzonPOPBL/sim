package principal;

/** @file Principal.java
* @brief Initialize the program and threads
* @authors
* Name | Surname     | Email                                 |
* -------------      | -----------------                     | ---------------------------------------------------|
* Aitor | Pi�eiro    | aitor.pineiro@alumni.mondragon.edu    |
* Ander | Rekalde    | ander.recalde@alumni.mondragon.edu    |
* Ander | Rementeria | ander.rementeria@alumni.mondragon.edu |
* Anton | Alberdi    | anton.alberdi@alumni.mondragon.edu    |
* @date 03/01/2019
*/
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import Almacen.Almacen;
import conectionMySQL.RobotConectionMySQL;
import controladores.ControladorPedidos;
import controladores.ControladorRobots;
import controladores.ControladorRuta;
import controladores.ControladorSegmento;
import listas.ListaSegmento;
import modelo.all.Robot;
import modelo.all.Segmento;
import modelo.all.WorkStation;

public class Principal implements PropertyChangeListener {

	RobotConectionMySQL robotConection;
	List<Robot> listaRobots;
	List<WorkStation> listaWk;
	ListaSegmento listaSegmentos;

	

	ControladorRobots controladorRobots;
	ControladorSegmento controladorSegmentos;
	ControladorPedidos controladorPedidos;
	ControladorRuta rutero;

	/**Constructor that initializes the segments, the robots , 
	*the controller and the connection to the database
	*@param No parameter
	*@return Constructor
	*/
	public Principal() {
		
		crearSegmentos();
		crearRobots();
		controladorPedidos = new ControladorPedidos(this);
		robotConection = new RobotConectionMySQL();
	}

	/**A method that initializes 6 robots
	*@param No parameter
	*@return Void method
	*/
	public void crearRobots() {
		listaRobots = new ArrayList<Robot>();
		for (int i = 1; i <= 6; i++) {
			listaRobots.add(new Robot(i, "parado", listaSegmentos.getListaSegmentos(), rutero));
		}
		controladorRobots = new ControladorRobots(listaRobots, this);
	}

	/**Method that initializes the 10 segments
	*@param No parameter
	*@return Void method
	*/
	public void crearSegmentos() {
		crearWorkStations();
		listaSegmentos = new ListaSegmento();
		for (int i = 1; i <= 10; i++) {
			if (i < 9) listaSegmentos.add(new Segmento(i, null, listaWk.get(i - 1)));
			else listaSegmentos.add(new Segmento(i, null, null));
		}
		controladorSegmentos = new ControladorSegmento(listaSegmentos.getListaSegmentos(), this);
		enlazarWk();
	}
	
	/** Method that  assigns segments to its workstation
	*@param No parameter
	*@return Void method
	*/
	public void enlazarWk() {
		int cont = 0;
		for (Segmento s : listaSegmentos.getListaSegmentos()) {
			if (cont < 8) s.getWorkstation().setIdSegmento(s.getsId());
			cont++;
		}
	}

	/**Method that creates the workstations
	*@param No parameter
	*@return Void method
	*/
	public void crearWorkStations() {
		boolean park = true;
		listaWk = new ArrayList<WorkStation>();
		int z = 1;
		for (int i = 1; i <= 8; i++) {
			if (park) {
				listaWk.add(new WorkStation("p0" + z, park));
				park = false;
			} else {
				listaWk.add(new WorkStation("10" + z, park));
				park = true;
				z++;
			}
		}
	}

	public List<Robot> getListaRobots() {
		return listaRobots;
	}

	public ListaSegmento getListaSegmentos() {
		return listaSegmentos;
	}

	public void gestionarThreads() {
		controladorPedidos.start();
	}

	public static void main(String[] args) {
		Principal programa = new Principal();
		programa.gestionarThreads();
	}

	/** Method that updates the status of a robot or a position
	*@param PropertyChangeEvent e
	*@return Void method
	*/
	@Override
	public void propertyChange(PropertyChangeEvent e) {

		switch (e.getPropertyName()) {
		case "1":
			robotConection.updateRobot(controladorRobots.getListaRobots().get(0));
			break;
		case "2":
			robotConection.updateRobot(controladorRobots.getListaRobots().get(1));

			break;
		case "3":
			robotConection.updateRobot(controladorRobots.getListaRobots().get(2));

			break;
		case "4":
			robotConection.updateRobot(controladorRobots.getListaRobots().get(3));
			break;
		case "5":
			robotConection.updateRobot(controladorRobots.getListaRobots().get(4));
			break;
		case "P1":
			robotConection.updatePosicion(controladorRobots.getListaRobots().get(0).setPosicion(),
					controladorRobots.getListaRobots().get(0));
			break;
		case "P2":
			robotConection.updatePosicion(controladorRobots.getListaRobots().get(1).setPosicion(),
					controladorRobots.getListaRobots().get(1));
			break;
		case "P3":
			robotConection.updatePosicion(controladorRobots.getListaRobots().get(2).setPosicion(),
					controladorRobots.getListaRobots().get(2));
			break;
		case "P4":
			robotConection.updatePosicion(controladorRobots.getListaRobots().get(3).setPosicion(),
					controladorRobots.getListaRobots().get(3));
			break;
		case "P5":
			robotConection.updatePosicion(controladorRobots.getListaRobots().get(4).setPosicion(),
					controladorRobots.getListaRobots().get(4));
			break;
		case "P6":
			robotConection.updatePosicion(controladorRobots.getListaRobots().get(5).setPosicion(),
					controladorRobots.getListaRobots().get(5));
			break;
	
		}
		
	}
	public ControladorRobots getControladorRobots() {
		return controladorRobots;
	}
}

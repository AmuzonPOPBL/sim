package modelo.all;
/** @file Segmento.java
* @brief Segment object definition
* @authors
* Name | Surname     | Email                                 |
* -------------      | -----------------                     | ---------------------------------------------------|
* Aitor | Pi�eiro    | aitor.pineiro@alumni.mondragon.edu    |
* Ander | Rekalde    | ander.recalde@alumni.mondragon.edu    |
* Ander | Rementeria | ander.rementeria@alumni.mondragon.edu |
* Anton | Alberdi    | anton.alberdi@alumni.mondragon.edu    |
* @date 03/01/2019
*/
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import modelo.all.WorkStation;

public class Segmento {

	int sId;
	boolean isFull;
	Robot robotEnSeg;
	WorkStation workstation = null;

	private PropertyChangeSupport propertySupport;

	public Segmento(int sId, Robot robotEnSeg, WorkStation wkS) {
		super();
		this.sId = sId;
		this.isFull = false;
		this.robotEnSeg = robotEnSeg;
		this.workstation = wkS;
		propertySupport = new PropertyChangeSupport(this);
	}

	/**Method that tries to enter a robot in a segment. if it is full the robot will wait
	*@param Robot robot
	*@return Void method
	*/
	public synchronized void getIn(Robot robot) {
		while (isFull) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		this.robotEnSeg = robot;
		this.setFull(true);
	}
	
	public Segmento() {
		this.isFull = false;
		propertySupport = new PropertyChangeSupport(this);
	}

	/**Method that takes out a robot from a segment
	*@param No parameter
	*@return Void method
	*/
	public synchronized void getOut() {
		this.robotEnSeg = null;
		this.setFull(false);
		notify();
	}

	public Robot getRobotEnSeg() {
		return robotEnSeg;
	}

	public void setRobotEnSeg(Robot robotEnSeg) {
		this.robotEnSeg = robotEnSeg;
	}

	public int getsId() {
		return sId;
	}

	public void setsId(int sId) {
		this.sId = sId;
	}

	public WorkStation getWorkstation() {
		return workstation;
	}

	public void setWorkstation(WorkStation workstation) {
		this.workstation = workstation;
	}

	public boolean isFull() {
		return isFull;
	}
	/**Method that sets a segment as full, this is, no more robot can enter until its empty
	*@param boolean isFull
	*@return Void method
	*/
	public void setFull(boolean isFull) {
		boolean old = this.isFull;
		boolean newValue = this.isFull = isFull;
		propertySupport.firePropertyChange("Segmento", old, newValue);
	}

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		propertySupport.addPropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		propertySupport.removePropertyChangeListener(listener);
	}
}
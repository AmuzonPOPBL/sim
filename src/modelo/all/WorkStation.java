package modelo.all;
/** @file WorkStation.java
* @brief Workstation object definition
* @authors
* Name | Surname     | Email                                 |
* -------------      | -----------------                     | ---------------------------------------------------|
* Aitor | Pi�eiro    | aitor.pineiro@alumni.mondragon.edu    |
* Ander | Rekalde    | ander.recalde@alumni.mondragon.edu    |
* Ander | Rementeria | ander.rementeria@alumni.mondragon.edu |
* Anton | Alberdi    | anton.alberdi@alumni.mondragon.edu    |
* @date 03/01/2019
*/
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class WorkStation {
	int idBD;
	String Id;
	int idSegmento;
	boolean parking;
	boolean isFull;
	Robot robotEnWSt;

	private PropertyChangeSupport propertySupport;

	public WorkStation(String Id, boolean park) {
		this.Id = Id;
		isFull = false;
		this.parking = park;
		propertySupport = new PropertyChangeSupport(this);
	}

	/**Method that tries to enter a robot in a workstation
	*@param Robot robot
	*@return Void method
	*/
	public synchronized void getIn(Robot robot) {
		while (isFull) {
			if ((robot.getOrden().equals(this.getId())
					|| robot.getDestino().equals(this.getId()))) {
				if (robotEnWSt.getRobotState().equals("parado")) {
					this.despertar();
					try {
						wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				} else {
					try {
						wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}else {
				try {
					wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		this.robotEnWSt = robot;
		this.setFull(true);

	}

	public synchronized void getOut() {
		this.robotEnWSt = null;
		this.setFull(false);
		notifyAll();

	}

	/**Method that takes out a robot from a workstation
	*@param No parameter
	*@return Void method
	*/
	public synchronized void descanso(Robot robot) {
		try {
			wait();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		robot.setRobotState("parkingBusca");
	}

	public synchronized void despertar() {
		notify();
	}

	public String getId() {
		return Id;
	}

	public boolean isFull() {
		return isFull;
	}

	/**Method that sets a workstation as full, this is, no more robot can enter until its empty
	*@param boolean isFull
	*@return Void method
	*/
	public void setFull(boolean isFull) {
		boolean old = this.isFull;
		boolean newValue = this.isFull = isFull;
		propertySupport.firePropertyChange("WorkStation", old, newValue);
	}

	public Robot getRobotEnWSt() {
		return robotEnWSt;
	}

	public void setRobotEnWSt(Robot robotEnWSt) {
		this.robotEnWSt = robotEnWSt;
	}

	public boolean isParking() {
		return parking;
	}

	public void setParking(boolean parking) {
		this.parking = parking;
	}

	public int getIdSegmento() {
		return idSegmento;
	}

	public void setIdSegmento(int idSegmento) {
		this.idSegmento = idSegmento;
	}

	@Override
	public String toString() {
		return "WorkStation : " + Id;
	}

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		propertySupport.addPropertyChangeListener(listener);

	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		propertySupport.removePropertyChangeListener(listener);
	}
}

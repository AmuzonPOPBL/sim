package modelo.all;
/** @file Robot.java
* @brief Robot object definition
* @authors
* Name | Surname     | Email                                 |
* -------------      | -----------------                     | ---------------------------------------------------|
* Aitor | Pi�eiro    | aitor.pineiro@alumni.mondragon.edu    |
* Ander | Rekalde    | ander.recalde@alumni.mondragon.edu    |
* Ander | Rementeria | ander.rementeria@alumni.mondragon.edu |
* Anton | Alberdi    | anton.alberdi@alumni.mondragon.edu    |
* @date 03/01/2019
*/
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;

import controladores.ControladorRuta;

public class Robot extends Thread {
	int rId;
	String orden = null;
	String destino = null;
	String robotState = null;
	int positionID;
	WorkStation wkActual = null;
	Segmento segActual = null;
	List<Segmento> rutas;
	List<Segmento> mapa;
	ControladorRuta rutero;
	private PropertyChangeSupport propertySupport;

	public Robot(int rId, String robotState, List<Segmento> mapa, ControladorRuta rutero) {
		super();
		this.rId = rId;
		this.robotState = robotState;
		this.mapa = mapa;
		this.positionID = rId;
		this.rutero = rutero;
		rutas = new ArrayList<Segmento>();
		propertySupport = new PropertyChangeSupport(this);
	}

	public Robot() {
		propertySupport = new PropertyChangeSupport(this);
	}

	/**Method that makes a robot to move from one segmento to another
	*@param Segmento s
	*@return Void method
	*/
	public synchronized void move(Segmento s) {
		boolean end = false;

		if (s.getWorkstation() != null) {
			if (s.getWorkstation().getId().equals(orden)) {
				s.getIn(this);
				s.getWorkstation().getIn(this);
				if (wkActual != null) wkActual.getOut();
				if (segActual != null)segActual.getOut();
				segActual = s;
				wkActual = segActual.getWorkstation();
			}
			if (s.getWorkstation().getId().equals(destino)) {
				s.getIn(this);
				s.getWorkstation().getIn(this);
				if (wkActual != null) wkActual.getOut();
				if (segActual != null) segActual.getOut();
				s.getOut();
				segActual = null;
				wkActual = s.getWorkstation();
				end = true;
			}
		}
		if ((s.getWorkstation() == null || !s.getWorkstation().getId().equals(orden)) && !end) {
			s.getIn(this);
			if (segActual != null) segActual.getOut();
			if (wkActual != null) wkActual.getOut();
			wkActual = null;
			segActual = s;
			waitMS(2000);
		}
	}
	/**Method that stops the execution for a time
	*@param int ms
	*@return Void method
	*/
	public void waitMS(int ms) {
		try {
			sleep(ms);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public Segmento getSegActual() {
		return segActual;
	}

	public void setSegActual(Segmento segActual) {
		this.segActual = segActual;
	}

	/**Method that makes the robot change its position, sleep it or make him park.
	*@param No parameter
	*@return Void method
	*/
	public void run() {
		while (true) {
			if (this.getRobotState().equals("parado")) {
				if (wkActual != null) {
					wkActual.descanso(this);
					if (rutas != null) this.setRobotState("moviendose");
					else this.setRobotState("parkingBusca");
				}
			}

			if (this.getRobotState().equals("moviendose")) {
				for (Segmento s : rutas) {
					move(s);
					this.setPosicion2();
					System.out.println("====================================================");
					if (wkActual == null) System.out.println("Robot" + this.rId + " posicion: " + segActual.getsId());
					else System.out.println(
							"Robot" + this.rId + " posicion: " + wkActual.getIdSegmento() + " " + wkActual);
					System.out.println("====================================================");
				}
				this.setRobotState("parado");
				rutas = null;
				System.out.println("Robot " + this.rId + "Termin�");
				rutero.incKont();
				rutero.checkBarrier();
			}
			if (this.getRobotState().equals("parkingBusca")) buscarParking();
		}

	}
	/**Method that makes a robot find a parking. if there is no parking free it will continue moving
	*@param No parameter
	*@return Void method
	*/
	public synchronized void buscarParking() {
		boolean end = false;
		int origen = wkActual.getIdSegmento();
		Segmento segmentoAux;
		System.out.println(this.getrId() + "Buscando parking");

		while (!end) {
			if (origen < 8) {
				segmentoAux = mapa.get(origen);
				if (segmentoAux.getWorkstation().isParking() && (!segmentoAux.getWorkstation().isFull())) {
					segmentoAux.getIn(this);
					if (wkActual != null) wkActual.getOut();
					if (segActual != null)segActual.getOut();
					segmentoAux.getWorkstation().getIn(this);
					segActual = null;
					wkActual = segmentoAux.getWorkstation();
					segmentoAux.getOut();
					this.setPosicion2();
					waitMS(1000);
					end = true;
				} else {
					segmentoAux.getIn(this);

					if (wkActual != null) {
						wkActual.getOut();
					}
					if (segActual != null) {
						segActual.getOut();
					}
					segActual = segmentoAux;
					this.setPosicion2();
				}
				origen++;
			} else {
				origen = 0;
			}
		}
		this.setRobotState("parado");
	}

	public void setRuta(List<Segmento> ruta) {
		this.rutas = ruta;
	}

	public int getrId() {
		return rId;
	}

	@Override
	public String toString() {
		return "Robot [rId=" + rId + ", robotState=" + robotState + ", positionID=" + positionID + ", segActual="
				+ segActual + ", rutas=" + rutas + "]";
	}

	public String getRobotState() {
		return robotState;
	}

	public String getOrden() {
		return orden;
	}

	public void setOrden(String orden) {
		this.orden = orden;
	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public void setRobotState(String robotState) {
		String old = this.robotState;
		this.robotState = robotState;
		propertySupport.firePropertyChange("" + this.rId, old, this.robotState);
	}

	/**Method that sets a position to a robot
	*@param No parameter
	*@return Void method
	*/
	public String setPosicion() {
		String sentence;
		if (!(this.segActual == null)) {
			if (!(this.wkActual == null)) {
				sentence = "id_sg=" + this.segActual.getsId() + ", id_ws='" + this.wkActual.getId() + "'";
			} else {
				sentence = "id_sg=" + this.segActual.getsId() + ", id_ws=" + null;
			}
		} else {
			sentence = "id_sg= " + null + ", id_ws= " + null;
		}

		return sentence;
	}

	public void setPosicion2() {
		String old = this.robotState;
		propertySupport.firePropertyChange("P" + this.rId, old, "eioi");
	}

	public void setOrden(String orden, String destino) {
		this.orden = orden;
		this.destino = destino;
		this.setRobotState("moviendose");
	}

	public int getPositionID() {
		return positionID;
	}

	public void setPositionID(int positionID) {
		int old = this.positionID;
		this.positionID = positionID;
		propertySupport.firePropertyChange("" + this.rId, old, this.positionID);
	}

	public WorkStation getWkActual() {
		return wkActual;
	}

	public void setWkActual(WorkStation wkActual) {
		this.wkActual = wkActual;
	}

	public ControladorRuta getRutero() {
		return rutero;
	}

	public void setRutero(ControladorRuta rutero) {
		this.rutero = rutero;
	}

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		propertySupport.addPropertyChangeListener(listener);

	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		propertySupport.removePropertyChangeListener(listener);
	}

}
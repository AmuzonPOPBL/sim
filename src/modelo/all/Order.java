package modelo.all;
/** @file Order.java
* @brief Order object definition
* @authors
* Name | Surname     | Email                                 |
* -------------      | -----------------                     | ---------------------------------------------------|
* Aitor | Pi�eiro    | aitor.pineiro@alumni.mondragon.edu    |
* Ander | Rekalde    | ander.recalde@alumni.mondragon.edu    |
* Ander | Rementeria | ander.rementeria@alumni.mondragon.edu |
* Anton | Alberdi    | anton.alberdi@alumni.mondragon.edu    |
* @date 03/01/2019
*/
public class Order {
	int idOrder,idProduct;
	String destino;
	
	public Order(int idOrder,int idProducto,String destino) {
		this.idOrder=idOrder;
		this.idProduct=idProducto;
		this.destino=destino;
	}

	public int getIdOrder() {
		return idOrder;
	}

	public void setIdOrder(int idOrder) {
		this.idOrder = idOrder;
	}

	public int getIdProducto() {
		return idProduct;
	}

	public void setIdProducto(int idProducto) {
		this.idProduct = idProducto;
	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}
	
}

package conectionMySQL;

/** @file RobotConectionMySQL.java
* @brief Conection to database and queries for robots
* @authors
* Name | Surname     | Email                                 |
* -------------      | -----------------                     | ---------------------------------------------------|
* Aitor | Pi�eiro    | aitor.pineiro@alumni.mondragon.edu    |
* Ander | Rekalde    | ander.recalde@alumni.mondragon.edu    |
* Ander | Rementeria | ander.rementeria@alumni.mondragon.edu |
* Anton | Alberdi    | anton.alberdi@alumni.mondragon.edu    |
* @date 03/01/2019
*/

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import modelo.all.Robot;

public class RobotConectionMySQL {
	static Connection connection = null;
	static Statement statement = null;
	String serverName = "localhost";
	String dataBaseName = "usuarios";
	String url = "jdbc:mysql://";
	static String username = "root";
	static String password = "1234";
	String connectionString = null;
	
	
	public RobotConectionMySQL(){
		this.connectionString = url + serverName + "/" + dataBaseName;
	}
	/**Creates the connection with the database
	*@param No parameter
	*@return Void method
	*/
	private void connect() {
		try {

			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection(connectionString,
					username, password);
			statement = connection.createStatement();
		} catch (ClassNotFoundException e) {

			System.out.println("Connection Driver Error");
		} catch (SQLException e) {

			e.printStackTrace();
			System.out.println("Could Not Connect to DB ");
		}
	}

	/** Destroy connection with database
	*@param No parameter
	*@return Void method
	*/
	private void disconnect() {
		try {
			if (statement != null) {
				statement.clearWarnings();
				statement.close();
			}
			if (connection != null) {
				connection.clearWarnings();
				connection.close();
			}
		} catch (SQLException e) {
			System.out.println("Error disconnecting");
		}
	}
	

	/**Connect to database and Queries the database to update a robot. Finally destroys the connection
	*@param Robot r
	*@return Boolean
	*/
	public synchronized boolean updateRobot(Robot r) {
		boolean ret = false;
		String sqlQuery = "UPDATE robot "+
		"SET "
		+ "robot_status = '"+r.getRobotState()
		+ "' WHERE id = "+r.getrId();
		System.out.println(sqlQuery);
		
		connect();
		try{
			Statement stm = connection.createStatement();
			if(stm.executeUpdate(sqlQuery)>0){
				 ret = true;
			}
		}catch(SQLException e){
			e.printStackTrace();
			
		}
		disconnect();
		return  ret;
	}
	

	/**Connect to database and Queries the database to update a position. Finally destroys the connection
	*@param String s, Robot r
	*@return Boolean
	*/
	public synchronized boolean updatePosicion(String s, Robot r) {
		boolean ret = false;
		String sqlQuery = "UPDATE position "+
		"SET "
		+ s + " WHERE id = "+r.getrId();
		System.out.println(sqlQuery);
		
		connect();
		try{
			Statement stm = connection.createStatement();
			if(stm.executeUpdate(sqlQuery)>0){
				ret=true;
			}
		}catch(SQLException e){
			e.printStackTrace();
			
		}
		disconnect();
		return ret;
	}
}

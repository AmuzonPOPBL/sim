package conectionMySQL;

/** @file OrderConectionMySQL.java
* @brief Connection to database and queries for orders
* @authors
* Name | Surname     | Email                                 |
* -------------      | -----------------                     | ---------------------------------------------------|
* Aitor | Pi�eiro    | aitor.pineiro@alumni.mondragon.edu    |
* Ander | Rekalde    | ander.recalde@alumni.mondragon.edu    |
* Ander | Rementeria | ander.rementeria@alumni.mondragon.edu |
* Anton | Alberdi    | anton.alberdi@alumni.mondragon.edu    |
* @date 03/01/2019
*/
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import modelo.all.Order;

public class OrderConectionMySQL {
	static Connection connection = null;
	static Statement statement = null;
	String serverName = "localhost";
	String dataBaseName = "usuarios";
	String url = "jdbc:mysql://";
	static String username = "root";
	static String password = "1234";
	static String connectionString = null;

	@SuppressWarnings("static-access")
	public OrderConectionMySQL() {
		this.connectionString = url + serverName + "/" + dataBaseName;
	}
	/**Creates the connection with the database
	*@param No parameter
	*@return Void method
	*/
	public static void connect() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection(connectionString, username, password);
			statement = connection.createStatement();
		} catch (ClassNotFoundException e) {

			System.out.println("Connection Driver Error");
		} catch (SQLException e) {

			e.printStackTrace();
			System.out.println("Could Not Connect to DB ");
		}
	}

	/** Destroy connection with database
	*@param No parameter
	*@return Void method
	*/
	public static void disconnect() {
		try {

			if (statement != null) {

				statement.clearWarnings();
				statement.close();
			}

			if (connection != null) {

				connection.clearWarnings();
				connection.close();
			}
		} catch (SQLException e) {

			System.out.println("Error disconnecting");
		}
	}

	/**Connect to database and Queries the database to obtain a list of all orders. Finally destroys the connection
	*@param No parameter
	*@return ArrayList<Order>
	*/
	public synchronized ArrayList<Order> loadAllOrders() {
		ArrayList<Order> orders = new ArrayList<Order>();
		int kont = 0;

		connect();
		ResultSet rs = null;
		String sqlQuery = "SELECT o.id,op.product_id,o.destination FROM orders AS o"
				+ " JOIN orders_products AS op ON op.order_id = o.id WHERE status='0';";
		int idO, idP;
		String dest;
		try {
			Statement stm = connection.createStatement();
			rs = stm.executeQuery(sqlQuery);
			while (rs.next()) {
				if (kont <= 5) {
					idO = (rs.getInt("id"));
					idP = (rs.getInt("product_id"));
					dest = (rs.getString("destination"));

					orders.add(new Order(idO, idP, dest));
					kont++;
				}
			}
		} catch (SQLException e) {
			System.out.println(sqlQuery);
			e.printStackTrace();
			System.out.println("Error OrdersMysql loadAllOrders");
		}
		disconnect();
		return orders;
	}

	/** Connect to database and queries to get the origin of the product. Finally destroys the connection.
	*@param Order o
	*@return String
	*/
	public synchronized String getOrigenProducto(Order o) {
		String origen = "";

		connect();
		ResultSet rs = null;
		String sqlQuery = "SELECT po.id_ws AS origen FROM product AS pr LEFT JOIN position AS po ON po.id = pr.position_id "
				+ "WHERE pr.id = '" + o.getIdProducto() + "';";
		try {
			Statement stm = connection.createStatement();
			rs = stm.executeQuery(sqlQuery);
			while (rs.next()) {
				origen = (rs.getString("origen"));
			}
		} catch (SQLException e) {
			System.out.println(sqlQuery);
			e.printStackTrace();
			System.out.println("Error OrdersMysql getOrigenProducto");
		}
		disconnect();
		return origen;
	}

	/** Method that connects to database and updates the orders when they are finished
	*@param Order o
	*@return Boolean
	*/
	public synchronized boolean updateFinishedOrders(Order o) {
		boolean ret = false;
		String sqlQuery = "UPDATE orders SET status = '1' WHERE id = '" + o.getIdOrder() + "';";
		System.out.println(sqlQuery);
		connect();
		try {
			Statement stm = connection.createStatement();
			if (stm.executeUpdate(sqlQuery) > 0) ret = true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		disconnect();
		return ret;
	}

	/**Connect to database and Queries the database to assign to robot the id of a order. Finally destroys the connection
	*@param Order o, int i
	*@return Boolean
	*/
	public synchronized boolean updateRobotOrder(Order o,int i) {
		boolean ret = false;

		String sqlQuery = "UPDATE orders SET robot_id = '"+ i +"' WHERE id = '" + o.getIdOrder() + "' AND robot_id IS NULL;";
		System.out.println(sqlQuery);

		connect();
		try {
			Statement stm = connection.createStatement();
			if (stm.executeUpdate(sqlQuery) > 0) {
				ret = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();

		}
		disconnect();
		return ret;
	}

}
